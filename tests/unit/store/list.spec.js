/* eslint-disable */

import { createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import axios from 'axios'

import { list } from '../../../src/modules/list';

let localVue,
    moduleList,
    store;

moduleList = list;

jest.mock('axios', () => {
  return {
    get: jest.fn().mockImplementation(() => {
      return new Promise((resolve) => resolve({data: {
          results: [{ name: "vader" }],
          count: 1
        }}))
    })
  }
});

//TODO pass store args to method to avoid huge code issue

const storeStartup = () => {
  localVue = createLocalVue();
  localVue.use(Vuex);

  store = new Vuex.Store({
    state: {},
    modules: {
      list: moduleList
    }
  });
};

describe('[Store] list', () => {
  beforeEach(() => {
    storeStartup()
  });

  describe('[Store.actions]', () => {
    describe('[Action] getPeople', () => {
      it('should call list of people', async () => {
        await store.dispatch('list/getPeople');

        expect(axios.get).toHaveBeenCalledTimes(1);

        expect(axios.get).toHaveBeenCalledWith('https://swapi.co/api/people/');

        expect(store.getters['list/counts']).toEqual(1);
      });
    })
  });

  describe('[Store getters]', () => {
    describe('[Getter] onlyMale', () => {
      beforeEach(() => {
        moduleList.state.people = [
          { name: 'Max', gender: 'male' },
          { name: 'Eugene', gender: 'male' },
          { name: 'Lily', gender: 'female' }
        ];

        storeStartup();
      });

      it('should return only male people', () => {
        expect(store.getters['list/onlyMale'].every(character => character.gender === 'male')).toBeTruthy();
      })
    })
  });

  describe('[Store mutations]', () => {
    describe('[Mutation] updatePeople', () => {
      beforeEach(() => {
        storeStartup();
      });

      it('should update list with new data', () => {
        const expectedResult = [
          { name: 'Alex Gray', gender: 'female' },
          { name: 'Pier Woodman', gender: 'female' },
        ];

        store.commit('list/updatePeople', expectedResult);

        expect(store.getters['list/people']).toEqual(expectedResult)
      })
    })
  })
});
