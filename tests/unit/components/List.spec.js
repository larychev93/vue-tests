/* eslint-disable */

import { createLocalVue, shallowMount } from '@vue/test-utils';
import List from '../../../src/components/List';
import ListItem from '../../../src/components/ListItem'
import Vuex from 'vuex';
import { list } from '../../../src/modules/list'

let localVue,
    wrapper,
    store;

describe('Component List', () => {
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);

    store = new Vuex.Store({
      state: {},
      modules: {
        list
      }
    });

    wrapper = shallowMount(List, {
      store, localVue
    });
  });

  it('should not render ListItem component if there are no people', () => {
    expect(wrapper.find(ListItem).exists()).toBe(false)
  });

  it('should render ListItem twice', () => {
    list.state.people = [
      { name: 'Max', gender: 'male' },
      { name: 'Eugene', gender: 'female' }
    ];

    store = new Vuex.Store({
      state: {},
      modules: {
        list
      }
    });

    wrapper = shallowMount(List, {
      store, localVue
    });

    expect(wrapper.findAll(ListItem).length).toBe(list.state.people.length)
  });

  it ('should render ListItem with correct props', () => {
    list.state.people = [
      { name: 'Max', gender: 'male' },
      { name: 'Eugene', gender: 'female' }
    ];

    store = new Vuex.Store({
      state: {},
      modules: {
        list
      }
    });

    wrapper = shallowMount(List, {
      store, localVue
    });

    const listItems = wrapper.findAll(ListItem);

    list.state.people.forEach((character, index) => {
      const item = listItems.at(index);

      expect(item.props().character).toBe(character)
    })
  })
});
