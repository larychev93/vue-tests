/* eslint-disable */

import flushPromises from 'flush-promises';
import { createLocalVue, shallowMount, mount } from '@vue/test-utils';
import CharacterForm from '../../../src/components/CharacterForm';
import { ValidationProvider } from 'vee-validate/dist/vee-validate.full';

let wrapper;


describe('Component CharacterForm',() => {
  beforeEach(() => {
    const localVue = createLocalVue();

    localVue.component('ValidationProvider', ValidationProvider);

    wrapper = mount(CharacterForm, {
      localVue,
      sync: false
    });
  });

  it('should emit event after name input fires', async () => {
    const expectedResult = {
      name: 'Link',
      gender: ''
    };

    const input = wrapper.find('[data-username]');

    input.setValue('Link');

    await flushPromises();

    expect(wrapper.emitted().setCharacter).toBeTruthy();

    expect(wrapper.emitted('setCharacter').length).toBe(1);

    expect(wrapper.emitted().setCharacter[0][0]).toEqual(expectedResult)
  });

  it ('should add class to input and display error message if input is empty', async () => {
    const input = wrapper.find('[data-username]');

    input.setValue('');
    await flushPromises();

    const errorEl = wrapper.find('[data-username-validation-error]');

    expect(input.classes('invalid-control')).toBeTruthy();
    expect(errorEl.text()).toContain('required');
  });

  it ('should change data after input triggered', () => {
    const input = wrapper.find('[data-test-reactive]');

    input.element.value = 'Link';

    input.trigger('input');

    expect(wrapper.vm.reactiveInput).toBe('Link')
  })
});
