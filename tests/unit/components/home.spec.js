/* eslint-disable */

//FIXME find elements by data-atr

import { createLocalVue, shallowMount } from '@vue/test-utils';
import Home from '../../../src/views/Home';
import Vuex from 'vuex';

import { list } from '../../../src/modules/list'

let localVue,
    wrapper,
    store;

jest.mock('axios');

describe('Component Home', () => {
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(Vuex);

    store = new Vuex.Store({
      state: {},
      modules: {
        list
      }
    });

    store.dispatch = jest.fn();

    wrapper = shallowMount(Home, {
      store, localVue
    });
  });

  it('should has correct Title by default', () => {
    const title = wrapper.find('.home__title');

    expect(title.text()).toBe('Characters list')
  });

  it('should change title after change button clicked', () => {
    const title = wrapper.find('.home__title');

    wrapper.find('.home__button-change').trigger('click');

    expect(wrapper.vm.pageTitle).toBe('Awesome Characters list');
    expect(title.text()).toBe('Awesome Characters list')
  });

  it('should call action when component mounted', async () => {
    expect(store.dispatch).toHaveBeenCalledWith(
      'list/getPeople'
    );
  })
});
