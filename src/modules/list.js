import axios from 'axios';

// eslint-disable-next-line import/prefer-default-export
export const list = {
  namespaced: true,
  state: {
    people: null,
    count: 0,
  },
  mutations: {
    updatePeople(state, people) {
      state.people = people;
    },

    updateCounter(state, counter) {
      state.count = counter;
    },
  },
  actions: {
    async getPeople({ commit }) {
      const { results, count } = (await axios.get('https://swapi.co/api/people/')).data;

      commit('updatePeople', results);
      commit('updateCounter', count);

      return results;
    },
  },
  getters: {
    people(state) {
      return state.people;
    },

    onlyMale(state) {
      return state.people && state.people.filter(character => character.gender === 'male');
    },

    counts(state) {
      return state.count;
    },
  },
};
