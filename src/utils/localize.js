import VueI18n from 'vue-i18n';
import en from '../lang/en.json';

const loadedLanguages = ['en'];

let i18n = null;

const init = (Vue) => {
  Vue.use(VueI18n);

  i18n = new VueI18n({
    locale: 'en', // set locale
    fallbackLocale: 'en',
    messages: {
      en,
    }, // set locale messages
  });

  return i18n;
};

const setI18nLanguage = (lang) => {
  i18n.locale = lang;
  document.querySelector('html').setAttribute('lang', lang);
  return lang;
};

const loadLanguageAsync = (lang) => {
  // If the same language
  if (i18n.locale === lang) {
    return Promise.resolve(setI18nLanguage(lang));
  }

  // If the language was already loaded
  if (loadedLanguages.includes(lang)) {
    return Promise.resolve(setI18nLanguage(lang));
  }

  // If the language hasn't been loaded yet
  return import(/* webpackChunkName: "lang-[request]" */ `@/lang/${lang}.json`).then(
    (translations) => {
      i18n.setLocaleMessage(lang, translations.default);

      loadedLanguages.push(lang);
      return setI18nLanguage(lang, translations);
    },
  );
};

export {
  init,
  setI18nLanguage,
  loadLanguageAsync,
};
