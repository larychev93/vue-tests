/* eslint-disable camelcase */
import { extend } from 'vee-validate';
import {
  required, min_value, max_value, length, email,
} from 'vee-validate/dist/rules';

// install the 'required' rule.
extend('required', {
  ...required,
  message: name => `${name} is required`,
});

extend('email', {
  ...email,
  message: () => 'Must be valid email',
});

extend('min_value', {
  ...min_value,
  message: (name, params) => `${name} must be greater than ${params.min}`,
});

extend('max_value', {
  ...max_value,
  message: (name, params) => `${name} must be lower than ${params.max}`,
});

extend('length', {
  ...length,
  message: (name, params) => `${name} field should have length of ${params.length}`,
});
