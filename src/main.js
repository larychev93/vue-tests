import Vue from 'vue';
import { ValidationProvider } from 'vee-validate/dist/vee-validate.full';
import App from './App.vue';
import router from './router';
import store from './store';
import { init } from './utils/localize';

import './utils/validations';

const i18n = init(Vue);

Vue.config.productionTip = false;

Vue.component('ValidationProvider', ValidationProvider);

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app');
